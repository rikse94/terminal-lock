<?php
use PhpGpio\Gpio;
function parseArgs($argArr){
    $out = array();
    foreach($argArr as $arg){

        if(substr($arg,0,2) == "--"){
            $stripped = split("=", substr($arg, 2));
            $key = $stripped[0];
            $value = $stripped[1];
            $out[$key] = $value;
        }

    }
    return $out;
}

function get($args, $key){
    return isset($args[$key]) ? $args[$key] : null;
}

$args = parseArgs($argv);

require("autoload.php");
$gpio = new GPIO();
$gpio->setup(18, "out");
$gpio->output(18, intval(get($args, "open")));
//$gpio->unexportAll();
?>
