<?php 

    require("autoload.php");
	require("BrowserStream.php");
	use coderofsalvation\BrowserStream;
	use PhpGpio\Gpio;

	
	BrowserStream::enable();
	
	BrowserStream::put("start\n");
	$last = "";
	$gpio = new GPIO();
	$gpio->setup(17, "in");
	
	for($i = 0; $i < 2000; $i++){			
		$new = $gpio->input(17);
		if($new != $last){
			BrowserStream::put("$new\n");
			$last = $new;		
		}
		usleep(1000*10);
	}
	echo BrowserStream::put("ok");
	

?>
