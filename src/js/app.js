requirejs.config({
    //By default load any module IDs from js/lib
    baseUrl: 'js/lib',

    paths: {
        app: '../app',
        misc: '../app/misc',
        Content: '../app/Content',
        templates: '../templates',
        jquery: 'jquery-1.12.4.min',
        text: 'require-text-2.1.20',
        jcolor: 'jquery.color'
    }
});

// Start the main app logic.
requirejs(['require', 'jquery', "declare", "util/wiring", "jcolor", "util/register"],
    function (require, $, declare, wiring) {
        app = this;
        console.log("SetCoorinator");
        wiring.setCoordinator(this);
        console.log("OK");
        
        uuid = function(){
            return 'xxxxxxxx-xxxx-4xxx-yxxx-xxxxxxxxxxxx'.replace(/[xy]/g, function(c) {var r = Math.random()*16|0, v = c == 'x' ? r : (r&0x3|0x8);return v.toString(16);})
        };

        function listenPowerStatus(){
            //console.log("Listening external power switch");
            var xhr = new XMLHttpRequest();
            xhr.open("GET", "test.php");
            xhr.onprogress = function(e) {
                var temp = e.currentTarget.responseText;
                var output;
                var match = temp.match(/(\d+)\n/g);
                if(match){
                    output = match[match.length - 1];
                    console.log(output);
                    wiring.broadcast("power.ext.toggle", {on: (output.substr(0,1))});
                }
            };
            xhr.onreadystatechange = function() {
                if (xhr.readyState == 4) {
                    //console.log("End of stream! Restarting...");
                    listenPowerStatus();
                }
            };
            xhr.send();
        }

        function resize() {
            var frame = $(".frame");
            frame.css("width", frame.height() * 1.333);
            frame.css("left", ($(document).width() / 2) - (frame.width() / 2));
        }

        $(document).ready(function () {
            resize();
        });
        $(window).resize(function () {
            resize()
        });
        

        require(["Content/loader"], function(loader){
            listenPowerStatus();
        });
            
    });