/**
 * Created by kuusri on 6/8/2016.
 */
define("app/misc/PowerSwitch", ["declare", "image!./images/on.png", "image!./images/off.png", "util/wiring"], function(declare, on, off, wiring){
    return declare("app.misc.PowerSwitch",[wiring], {
        postCreate: function(){
            var self = this;
                                                                                                                self.domNode.innerHTML = off.outerHTML;
            $(this.domNode).click(function(){
                self.switch();
            });
            self.listen("power.ext.toggle", "switch", self);
            
        },
        domNode: $("#powerSwitch")[0],
        state: false,
        switch: function(e, self){
            console.log(e);
            var toggle = true;
            if(!self){
                self = this;
            }

            if(e && e.on !== undefined){
                toggle = false;
                if(self.state != e.on){
                    this.state = !e.on;
                    toggle = true;
                }
            }
            if(toggle){
                self.state = !self.state;
                self.domNode.innerHTML = self.img[self.state].outerHTML;
                self.broadcast("power.toggle", {on: self.state});
            }

        },
        img: {
            true: on,
            false: off
        }
    });
});
