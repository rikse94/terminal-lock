define("app/about_en", function(){
    return {
        message: "This terminal is for personnel use only.\nAll unauthorized access is strictly prohibited.\n" +
        "If you have acquired anything even remotely relative to this terminal without SAMK supervisors written " +
        "permission, please refrain from all interactions with this terminal.",
        copyright: "&copy;Riku Kuusisto 2016",
        version: "1.0.0"
    }
});