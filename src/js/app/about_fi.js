define("app/about_fi", function(){
    return {
        message: "Tämä pääte on vain henkilökunnan käyttössä.\nKaikenlainen luvaton operointi on ehdottomasti kielletty.\n" +
        "Mikäli olet saanut tähän päätteeseen liittyvää tietoa ilman Satakunnan Ammattikorkeakoulu Osakeyhtiön päättevastaavan kirjallista hyväksyntää, " +
        "ole hyvä ja pitäydy operoimasta tätä päätettä.\n\nPäätenumero: 59947726",
        copyright: "&copy;SAMK Industries 2016",
        version: "1.0.0"
    }
});