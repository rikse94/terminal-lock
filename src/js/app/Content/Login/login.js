define("Content/Login/login", ["parser", "declare", "text!./templates/login.html", "text!./password.txt", "util/wiring", "util/class-expansion"],
function(parser, declare, loginTemplate, pw, wiring){
    loginTemplate = parser(loginTemplate, {password: "Salasana"});
    return declare("Content.Login.login", [wiring], {
        domNode: $(loginTemplate)[0],
        postCreate: function(){
            this.listen("password.submit", "submit", this);
        },
        submit: function(){

            if($("#hiddenPwd").val().hashCode() == pw){
                if(arguments[1].broadcast)
                    arguments[1].broadcast("navigate", "puzzle");
            } else {
                console.log("incorrect password");
            }
        }
    });

})