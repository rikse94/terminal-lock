define("Content/Puzzle/puzzle", ["parser", "declare", "text!./templates/puzzle.html", "dom-construct", "util/wiring"],
    function (parser, declare, template, domConstruct, wiring) {

        return declare("Content.Puzzle.puzzle", [wiring], {
            domNode: $(template),
            attempts: 4,
            keyup: null,
            candies: [
                {type: 1, message: "Hämy poistettu!"},
                {type: 2, message: "Yritykset nollattu!"}

            ],
            postCreate: function () {
                this.listen("page.loaded", "pageLoaded", this);
                this.listen("page.closed", "pageClosed", this);
                var self = this;
                this.keyup = function (ev) {
                    var sel = $(".puzzle.select.selected");
                    if ([37, 38, 39, 40].includes(ev.keyCode)) {
                        if (sel.length == 0) {
                            sel = $(".puzzle.select").first();
                            sel.addClass("selected");
                            return;
                        } else {
                            sel.removeClass("selected");
                            if (sel.hasClass("tail")) {
                                self.getPrev(sel).children().last().removeClass("selected");

                            } else if (sel.hasClass("head")) {
                                self.getNext(sel).children().first().removeClass("selected");
                            }
                        }
                    }

                    var target;
                    switch (ev.keyCode) {
                        case 40:
                            var parent = sel.parent();
                            var index = parent.children().index(sel);
                            var parentLength = parent.children().length;
                            var nextLength = parent.next().next().children().length;
                            var thisPos = 0;
                            var pos = 0;


                            for (i = 0; i < parentLength; i++) {

                                var c = parent.children()[i];
                                thisPos += c.innerHTML.replace(/&.*?;/g, ".").length;
                                if (index == i)
                                    break;
                            }

                            if (nextLength == 0) {
                                nextLength = parent.parent().next().children().first().next().children().length;
                            }

                            for (i = 0; i < nextLength; i++) {


                                var c = self.getNext(sel).children()[i];

                                pos += c.innerHTML.replace(/&.*?;/g, ".").length;
                                if (pos >= thisPos) {
                                    target = $(c);
                                    break;
                                }
                            }

                            if (target === undefined) {
                                /* if (parent.parent().next().children()[0]) {
                                 target = parent.parent().next().children().first().next().first();
                                 } else {
                                 target = sel;
                                 }*/
                                target = sel;
                            }
                            break;
                        case 38:
                            var parent = sel.parent();
                            var index = parent.children().index(sel);
                            var parentLength = parent.children().length;
                            var prevLength = parent.prev().prev().children().length;
                            var thisPos = 0;
                            var pos = 0;


                            for (i = 0; i < parentLength; i++) {

                                var c = parent.children()[i];
                                thisPos += c.innerHTML.replace(/&.*?;/g, ".").length;
                                if (index == i)
                                    break;
                            }

                            if (prevLength == 0) {
                                prevLength = parent.parent().prev().children().last().children().length;
                            }

                            for (i = 0; i < prevLength; i++) {

                                var c = self.getPrev(sel).children()[i];

                                pos += c.innerHTML.replace(/&.*?;/g, ".").length;
                                if (pos >= thisPos) {
                                    target = $(c);
                                    break;
                                }
                            }

                            if (target === undefined) {
                                /*if (parent.parent().prev().children()[0]) {
                                 var id = parent.parent().prev().children().length;
                                 target = parent.parent().prev().children().last().first();
                                 } else {
                                 target = sel;
                                 }*/
                                target = sel;
                            }

                            break;
                        case 37:
                            var prev = sel.prev();
                            if (!prev[0]) {
                                var parent = sel.parent();
                                var container = parent.parent();
                                var index = container.children().index(parent);


                                if (container.prev()[0]) {

                                    prev = $(container.prev().children()[index]).children().last();
                                }
                                else {
                                    if ($(container.next().children()[index - 2]).children().length > 0) {
                                        prev = $(container.next().children()[index - 2]).children().last();
                                    } else {
                                        prev = container.next().children().last().children().last();
                                    }

                                }
                            }
                            target = prev;

                            break;
                        case 39:
                            var next = sel.next();
                            if (!next[0]) {
                                var parent = sel.parent();
                                var container = parent.parent();
                                var index = container.children().index(parent);
                                if (container.next()[0]) {
                                    next = $(container.next().children()[index]).children().first();
                                } else {
                                    if ($(container.prev().children()[index + 2]).children().length > 0) {
                                        next = $(container.prev().children()[index + 2]).children().first();
                                    } else {
                                        next = container.prev().children().first().next().children().first();
                                    }
                                }

                            }
                            target = next;

                            break;
                        case 13:
                            //self.select($(".puzzle.select.selected"));
                            self.select(sel);
                            break;
                        default:
                            //console.log(ev.keyCode);
                            return;
                    }
                    if (target) {
                        self.setSelected(target);
                    }
                }
            },

            startup: function () {
                console.log("Startup");
                $(".hideable").css({display: "inline-block"});
                var self = this;
                this.attempts = 4;
                this.updateAttempts();
                $(".puzzle.input").html("&gt;<span class='message'></span>");


                var xhr = new XMLHttpRequest();
                xhr.open("get", window.location.origin + "/terminal-lock" + "/dictionary.php?count=13");
                xhr.send();

                xhr.onreadystatechange = function () {
                    if (xhr.readyState == 4 && xhr.status == 200) {
                        //self.game(JSON.parse(xhr.responseText));
                        self.success();

                        $(window).bind("keyup", self.keyup);
                        $(".puzzle.select").on("mouseover", function () {
                            console.log("Hovering");
                            $(".puzzle.select.selected").removeClass("selected");
                            self.setSelected($(self));
                        })
                    }
                };
            },
            updateAttempts: function () {
                $("#attempts").empty();
                for (var i = 0; i < this.attempts; i++) {
                    //console.log("Add attempt");
                    domConstruct.create("div", {class: "attempt"}, "#attempts");
                }
                if (this.attempts == 0) {
                    this.lockOut();
                    var self = this;
                    setTimeout(function () {
                        self.clear();
                        self.pageLoaded({menu: Menu.PUZZLE}, self);
                    }, 30000);

                }
            },
            pageLoaded: function (evt, self) {
                //console.log(evt);
                if (evt.menu == Menu.PUZZLE) {
                    self.startup();

                    var select = $(".puzzle.select");

                    select.click(function () {
                        self.select($(".puzzle.select.selected"));
                    })
                }


            },
            pageClosed: function (evt, self) {
                console.log(evt);

                if (evt.previous == Menu.PUZZLE) {

                    $("#attempts").empty();
                    $(".puzzle.area").empty();
                }

            },
            game: function (dictionary) {

                console.log("Min: 0 Max: ", dictionary.length-1);
                var rand = Math.round(Math.random() * (dictionary.length - 1));
                console.log("Random: ", rand)
                this.correct = dictionary[rand];
                var characters = "!\"#%&/\\()=?{}[]@$^'-.,_:;<>|*+";
                var data = {
                    begin: null,
                    end: null,
                    increment: 12,
                    rowCount: 30,
                    wordLength: dictionary[0].length,
                    data: [],
                    setBegin: function () {
                        this.begin = Math.round(Math.random() * 50000).toString(16);
                        return true;
                    },
                    getAddress: function (/** int */ index) {
                        if (typeof index == "number") {
                            index = Math.round(index);
                            var begin = parseInt(this.begin, 16);
                            var addr = begin + this.increment * index;
                            return addr.toString(16);
                        }
                    },
                    length: function () {
                        return this.increment * this.rowCount;
                    },
                    add: function (input) {
                        this.data.push(input);
                    },
                    generate: function () {
                        this.clear();
                        for (i = 0; i < this.length(); i++) {
                            var index = Math.round(Math.random() * (characters.length - 1));
                            var char = characters[index];
                            this.add(char);
                        }

                        var scope = Math.round(this.length() / dictionary.length);

                        for (i = 0; i < dictionary.length; i++) {
                            var begin = scope * i;

                            var wordLength = dictionary[0].length;
                            var pos = begin + Math.round(Math.random() * (scope - wordLength * 2));
                            for (var j = 0; j < wordLength; j++) {
                                this.data[pos + j] = dictionary[i][j];
                            }
                        }

                    },
                    clear: function () {
                        this.data = [];
                    },
                    print: function (callback) {
                        var self = this;
                        //var w = window.open();
                        var container = document.createElement("div"),
                            left = document.createElement("div"),
                            right = document.createElement("div");
                        container.setAttribute("style", "display: flex");

                        this.generate();
                        var split = false;
                        for (var i = 0; i < this.rowCount; i++) {
                            var addr = this.getAddress(i);
                            var index = parseInt(addr, 16) - parseInt(this.begin, 16);
                            var label = "0x" + addr + " ",
                                out = "";
                            for (var j = index; j < index + this.increment; j++) {
                                if (this.data[j]) {
                                    out += this.data[j];
                                }
                            }
                            var outArr = [];
                            var pattern = /(\(((?![a-z]).)*?\)|\[((?![a-z]).)*?]|\{((?![a-z]).)*?}|<((?![a-z]).)*?>)|[a-z]+|(?![a-z])./g,
                                patternHiddenOnly = /(\(((?![a-z]).)*\)|\[((?![a-z]).)*]|\{((?![a-z]).)*}|<((?![a-z]).)*?>)/g;
                            var matches = out.match(pattern);


                            matches.forEach(function (match) {
                                //console.log("Mached hidden button: ", match);
                                if (match.match(/[a-z]+/g)) {
                                    //console.log("Length: ", match.length, " : ", self.wordLength);
                                    if (match.length < self.wordLength) {
                                        if (!split) {
                                            split = true;
                                            outArr.push("<span class='puzzle select choice head'>" + match.replace("<", "&lt;").replace(">", "&gt;") + "</span>");
                                        } else {
                                            split = false;
                                            outArr.push("<span class='puzzle select choice tail'>" + match.replace("<", "&lt;").replace(">", "&gt;") + "</span>");
                                        }
                                    } else {
                                        outArr.push("<span class='puzzle select choice'>" + match.replace("<", "&lt;").replace(">", "&gt;") + "</span>");
                                    }

                                } else if (match.length == 1) {
                                    outArr.push("<span class='puzzle select character'>" + match + "</span>");
                                } else {
                                    /*
                                     var subMatches = [];
                                     var sMatches = match.substr(1).match(patternHiddenOnly);
                                     if (sMatches)
                                     sMatches.forEach(function (sMatch) {
                                     subMatches.push(sMatch);
                                     var ssMatches = sMatch.substr(1).match(patternHiddenOnly);
                                     if (ssMatches)
                                     ssMatches.forEach(function (ssMatch) {
                                     subMatches.push(ssMatch);
                                     });
                                     });
                                     if (subMatches.length > 0)
                                     console.log(subMatches);
                                     */

                                    match.replace(pattern, "<span class='puzzle select hidden'>" + "\1" + "</span>");
                                    var str = "<span class='puzzle select hidden'>" + match.replace("<", "&lt;").replace(">", "&gt;") + "</span>";
                                    outArr.push(str);
                                }
                            });


                            //console.log(outArr);

                            var labelDom = document.createElement("span");
                            var outDom = document.createElement("span");
                            labelDom.setAttribute("class", "puzzle label");
                            outDom.setAttribute("class", "puzzle out");
                            labelDom.innerHTML = label;
                            outDom.innerHTML = outArr.join("");
                            if (i < this.rowCount / 2) {
                                left.appendChild(labelDom);
                                left.appendChild(outDom);
                            } else {
                                right.appendChild(labelDom);
                                right.appendChild(outDom);
                            }
                            //callback(label + out);
                        }
                        container.appendChild(left);
                        container.appendChild(right);
                        //w.document.body.appendChild(container);
                        //console.log(w.document.body);
                        return container;
                    }
                };

                data.setBegin();

                //$(".puzzle.area")[0].innerHTML = "Puzzle Luotu";
                var container = data.print(function (a) {
                    console.log(a)
                });
                $(".puzzle.area")[0].appendChild(container);

            },

            clear: function () {
                this.domNode.find(".puzzle.area").empty();
                this.domNode.find("#attempts").empty();
                this.domNode.find(".puzzle.feedback").empty();
                this.domNode.find(".puzzle.input").empty();
                try {
                    $(window).unbind("keyup", this.keyup);
                } catch (ex) {

                }

            },

            select: function (el) {
                if (el.length == 0)
                    return;

                if (!el.hasClass("used")) {
                    var selectedWord = "";

                    if (!el.hasClass("character")) {
                        el.addClass("used");

                        if (!el.hasClass("tail") && !el.hasClass("head")) {
                            selectedWord = el.html();
                            console.log("no head, no tail..");
                            el.html(el.html().replace(/&.*?;|./g, "."));

                        }

                        console.log(el);
                        if (el.hasClass("head") && el.hasClass("tail") && el.length == 2) {
                            var head = $(el[0]),
                                tail = $(el[1]);
                            selectedWord = head.html() + tail.html();

                            console.log(selectedWord);

                            head.html(head.html().replace(/./g, "."));
                            tail.html(tail.html().replace(/./g, "."));
                            el.addClass("used");

                        }
                    }


                    if (el.hasClass("choice")) {
                        //alert("choice: " + el.html());
                        this.addFeedback(selectedWord);
                        if (selectedWord == this.correct) {
                            this.addFeedback("Oikea sana!");
                            this.success();
                        } else {
                            var count = 0;
                            for (i = 0; i < this.correct.length; i++) {
                                if (this.correct[i] == selectedWord[i]) {
                                    count++;
                                }
                            }
                            this.addFeedback("Täsmäävyys=" + count);

                            this.attempts--;
                            this.updateAttempts();

                        }

                    } else if (el.hasClass("hidden")) {

                        var candy = this.candies[Math.round(Math.random())];
                        this.addFeedback(candy.message);

                        switch (candy.type) {
                            case 1:
                                var choices = $(".puzzle.select.choice");
                                while (true) {
                                    var id = Math.round(Math.random() * (choices.length - 1));

                                    if (id < 0) return;

                                    var c = choices[id];

                                    if (c.innerHTML != this.correct && !c.classList.contains("used")) {
                                        c.classList.add("used");
                                        c.innerHTML = c.innerHTML.replace(/./g, ".");
                                        break;
                                    }
                                }
                                break;
                            case 2:
                                this.attempts = 4;
                                this.updateAttempts();
                                break;
                        }


                    }

                }
            },
            setSelected: function (target) {
                target.addClass("selected");
                if (target.hasClass("tail")) {
                    this.getPrev(target).children().last().addClass("selected");

                } else if (target.hasClass("head")) {
                    this.getNext(target).children().first().addClass("selected");
                }

                var el = $(".puzzle.selected");
                $("#puzzle_input").empty();
                $("#puzzle_input").html("> ");
                if (!el.hasClass("used")) {
                    var selectedWord = "";

                    if (!el.hasClass("character")) {
                        if (!el.hasClass("tail") && !el.hasClass("head")) {
                            selectedWord = el.html();
                        }

                        if (el.hasClass("head") && el.hasClass("tail") && el.length == 2) {
                            var head = $(el[0]),
                                tail = $(el[1]);
                            selectedWord = head.html() + tail.html();
                        }

                        this.showText("#puzzle_input", selectedWord, 0, 30);

                    }
                }
                

            },

            getNext: function (target) {
                var out = target.parent().next().next();
                if (out.length == 0) {
                    out = target.parent().parent().next().children().first().next();
                }
                //console.log({tail: out});
                return out;
            },

            getPrev: function (target) {
                var out = target.parent().prev().prev();
                if (out.length == 0) {
                    out = target.parent().parent().prev().children().last();
                }
                //console.log({head: out});
                return out;
            },

            addFeedback: function (message) {
                $(".puzzle.feedback").append("<div>&gt;" + message + "</div>");
            },

            lockOut: function () {
                this.clear();
                $(".hideable").css({display: "none"});
                var lockText = domConstruct.create("div", {
                    style: "width: 40rem; left: 50%; top: 45%; margin-left: -18rem; position: fixed;"
                }, ".puzzle.area");
                this.showText(lockText, "Pääte lukittu. Ole hyvä ja odota 30 sekuntia!", 0, 30);
            },
            success: function () {
                this.clear();
                $(".hideable").css({display: "none"});
                var successText = domConstruct.create("div", {
                    style: "width: 30rem; left: 50%; top: 45%; margin-left: -13rem; position: fixed;"
                }, ".puzzle.area");
                this.showText(successText, "Sormenjälkitunnistin käytössä!", 0, 30);

                /*
                 * Attention!!
                 * Trying to cheat, huh? You've deserved it :)
                 *
                 */


                var xhr = new XMLHttpRequest();
                xhr.open("get", window.location.origin + window.location.pathname + "gpio_handler.php?lock=open");
                xhr.send();
            },
            showText: function (target, message, index, interval, callback) {
                if(message === undefined)
                    return;
                var self = this;
                if (index < message.length) {
                    $(target).append(message[index++]);
                    if(message[index] == "\n"){
                        $(target).append($("<br />"));
                    }
                    setTimeout(function () {
                        self.showText(target, message, index, interval,
                            (callback) ? callback : null);
                    }, interval);
                } else if(callback){
                    callback();
                }
            }

        });

    });