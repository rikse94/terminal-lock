/**
 * Created by kuusri on 6/8/2016.
 */
define("Content/loader",
    ["declare",
        "dom-construct",
        "util/wiring",
        "util/event-handler",
        "util/hide-password",
        "./About/about",
        "./Login/login",
        "./Welcome/welcome",
        "./Puzzle/puzzle",
        "store/Memory",
        "app/misc/PowerSwitch"],
    function (declare, domConstruct, wiring, eventHandler, hidePassword, about, login, welcome, puzzle, Memory, PowerSwitch) {
        return declare("Content.loader", [hidePassword, eventHandler, wiring], {
            output: null,

            postCreate: function () {
                console.log("Loader PostCreate");
                this.output = $("#output")[0];
                this.listen("power.toggle", "togglePower", this);
                this.listen("navigate", "navigate", this);
            },

            navigate: function (page) {
                console.log("Navigating to ", page);
                

                var self = arguments[1] || this;
                switch (page) {
                    case "about":
                        self.display(about);
                        break;
                    case "login":
                        this.display(login, function(){
                            self.registerPassword($("#password")[0], $("#hiddenPwd")[0]);
                            self.passwordField.focus();
                        });

                        break;
                    case "welcome":
                        self.display(welcome);
                        break;
                    case "puzzle":
                        self.setCurrentMenu(Menu.PUZZLE);
                        self.display(puzzle);
                        break;
                    case "shutdown":
                        setTimeout((function () {
                            self.display({domNode: $("<div><p class='write'>Voit nyt sammuttaa päätteen.</p></div>")});
                        }), 1000);
                        break;

                }
            },

            display: function (content, callback) {
                if(content.clear)
                    content.clear();
                var self = this;
                var temp = new Memory();

                $(content.domNode).find(".write").each(function (index, el) {
                    var text = el.innerHTML;
                    el.innerHTML = "";
                    temp.put({el: el, innerHTML: text});
                });


                self.clearDisplay();
                domConstruct.placeAt($("<span id='header' class='hideable'></span>"), this.output);
                domConstruct.placeAt(content.domNode, this.output);


                //self.showText("#header", "Welcome to SAMK Industries (TM) Termlink", 0, 30, function () {
                self.showText("#header", "Tervetuloa SAMK Industries (TM) Termlink -päätteelle", 0, 30, function () {
                    var lastRow = null;
                    var items = {
                        rows : temp.query(),
                        index: 0,
                        hasNext: function(){
                            return this.rows[this.index + 1];
                        },
                        next: function(){

                            if(this.hasNext()){
                                this.index++;
                                var row = this.rows[this.index];
                                self.showText(row.el, row.innerHTML, 0, 30,
                                    (!this.hasNext() && this.callback) ? this.callback : null, (this.hasNext()) ? this : null);
                            } else {
                                return null;
                            }
                        },
                        callback: callback
                    };
                    var row = items.rows[0];
                    if(row)
                        self.showText(row.el, row.innerHTML, 0, 30, null, items);

                });
            },
            clearDisplay: function () {
                console.log("Clear Display");
                $(this.output).empty();
            },
            togglePower: function (e, self) {
                var frame = $(".frame");
                if (e.on) {
                    frame.animate({backgroundColor: "black"}, 1000, function () {
                        frame.animate({backgroundColor: "transparent"}, 200, function () {
                            $(".glow").css("display", "block");
                            self.displayMain();
                            self.coordination.participate(window, self);
                        });
                    });
                } else {
                    frame.animate({backgroundColor: "black"}, 200, function () {
                        self.clearDisplay();
                        self.coordination.currentMenu = Menu.MAIN;
                        self.coordination.previousMenu = null;
                        $(".glow").css("display", "none");
                    });
                }
            },
            showText: function (target, message, index, interval, callback, iterative) {
                ignoreKeyboard = true;
                var self = this;
                if (index < message.length) {
                    $(target).append(message[index++]);
                    if(message[index] == "\n"){
                        $(target).append($("<br />"));
                    }
                    setTimeout(function () {
                        self.showText(target, message, index, interval,
                            (callback) ? callback : null,
                            (iterative) ? iterative : null);
                    }, interval);
                } else {
                    if (callback)
                        callback();
                    if(iterative){
                        iterative.next();
                    } else {
                        if(target != "#header"){
                            ignoreKeyboard = false;
                            this.broadcast("page.loaded", {menu: this.coordination.currentMenu});
                        }

                    }
                }
            },
            login: function(e, self){
                self.displayPuzzle();
            }
        });
    });
