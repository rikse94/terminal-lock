define("Content/About/about", ["parser", "text!./templates/about.html", "app/about_fi"],
function(parser, template, strings){
    template = parser(template, strings);
    return {
        domNode: $(template)[0]
    }
})