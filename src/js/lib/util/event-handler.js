define("util/event-handler", ["declare", "dom-construct", "util/coordination", "util/wiring"], function (declare, domConstruct, coordination, wiring) {
    return declare("util.EventHandler", [], {
        coordination: null,
        preventEsc: false,
        postCreate: function () {
            console.log("EventHandler PostCreate");
            this.coordination = coordination;
            
        },
        handleEnter: function () {

            switch (this.coordination.currentMenu) {
                case Menu.MAIN:
                    this.mainMenuEnter();
                    break;
            }
        },

        handleEsc: function () {
            if(!this.preventEsc) {
                this.clearDisplay();
                this.unregisterPassword();
                this.displayMain();
                this.broadcast("page.closed", {previous: this.coordination.previousMenu});
            }
        },

        mainMenuEnter: function () {
            var current = this.coordination.buttons.getCurrent();
            if (current != null) {
                var value = $(current).attr("data-value");
                switch (value) {
                    case "login":
                        this.setPreviousMenu(Menu.MAIN);
                        this.displayLogin();
                        break;
                    case "about":
                        this.setPreviousMenu(Menu.MAIN);
                        this.displayAbout();
                        break;
                    case "exit":
                        this.setPreviousMenu(Menu.MAIN);
                        this.exit();
                        break;

                }
            }
        },

        setPreviousMenu: function(menu){
            for(var k in Menu){
                if(Menu[k] == menu)
                    this.coordination.previousMenu = menu;
            }

        },

        setCurrentMenu: function(menu){
            if(this.coordination.currentMenu){
                this.setPreviousMenu(this.coordination.currentMenu);
            }
            for(var k in Menu){
                if(Menu[k] == menu)
                    this.coordination.currentMenu = menu;
            }

        },

        displayMain: function () {
            if(this.coordination)
                this.setCurrentMenu(Menu.MAIN);
            this.navigate("welcome");
        },

        displayLogin: function () {
            this.setCurrentMenu(Menu.LOGIN);
            this.navigate("login");
        },

        displayPuzzle: function () {
            
            this.navigate("puzzle");
        },

        displayAbout: function () {
            this.setCurrentMenu(Menu.ABOUT);
            this.navigate("about");
        },

        exit: function () {
            this.navigate("shutdown");
        }
    });
});