define("util/hide-password", ["declare", "util/left-pad", "util/wiring"], function (declare, leftPad, wiring) {
    return declare("util.hidePassword", [wiring], {
        passwordField: null,
        registerPassword: function (password, hiddenPassword) {
            var pwd = $(password);
            var hid = $(hiddenPassword);
            var lastKey;
            var self = this;

            this.passwordField = pwd;
            this.hiddenField = hid;
            this.passwordField.on("keyup", function (ev) {

                if(ev.keyCode > 95 && ev.keyCode < 106){
                    ev.keyCode -= 48;
                }
                
                hid.val(hid.val().substr(0, pwd.val().length));
                if (ev.keyCode >= 48 && ev.keyCode <= 90) {
                    var value = hid.val() + String.fromCharCode(ev.keyCode + (ev.shiftKey ? 48 : 0));
                    hid.val(value);
                }

                if ((ev.keyCode == 17 && lastKey == 82)) {
                    var value = hid.val().substr(0, hid.val().length - 1);

                    hid.val(value);
                }
                if(ev.keyCode == 13){
                    self.broadcast("password.submit", {value: self.hiddenField.val()});
                }

                pwd.val(leftPad(hid.val().length, "*", ""));
                lastKey = ev.keyCode;
            });
        },
        unregisterPassword: function(){
            if(this.passwordField != null){
                this.passwordField.val("");
                this.passwordField.off("keyup");
            }
        },
        postCreate: function(){
            console.log("HidePassword PostCreate");
        }
    });
});