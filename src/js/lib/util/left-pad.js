define("util/left-pad", [], function(){
    return function (n, c, s){
        var out = "";
        for(i = 0; i < n; i++){
            out += c;
        }
        return out + s;
    }
})