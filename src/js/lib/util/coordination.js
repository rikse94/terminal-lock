define("util/coordination", [], function () {
    //Define menu enumeration
    Menu = {MAIN: 1, LOGIN: 2, ABOUT: 3, PUZZLE: 4};

    return {
        buttons: {
            previous: null,
            next: null,
            current: 0,
            getNext: function () {
                if (this.next == null) {
                    if (this.current != null) {
                        return $('.button')[this.current];
                    }
                } else {
                    return $('.button')[this.next];
                }
            },
            getPrevious: function () {
                if (this.previous == null) {
                    if (this.current != null) {
                        return $('.button')[this.current];
                    } else {
                        var b = $('.button');
                        return b[b.length - 1];
                    }
                } else {
                    return $('.button')[this.previous];
                }
            },
            getCurrent: function () {
                if (this.current != null) {
                    return $('.button')[this.current];
                }
            },
            setSelectedButton: function (button) {
                if (button == null) {
                    if (this.current != null) {
                        $(this.getCurrent()).removeClass("highlight");
                    }

                    this.current = null;
                } else {
                    var b = $('.button');
                    var index = b.index(button);
                    var length = b.length;

                    if (this.current != null) {
                        $(this.getCurrent()).removeClass("highlight");
                    }

                    $(button).addClass("highlight");
                    this.current = index;
                    this.next = index + ((length > index + 1) ? 1 : 0);
                    this.previous = index - ((index != 0) ? 1 : 0);
                }
            }
        },
        previousMenu: null,
        currentMenu: Menu.MAIN,


        isNavigationEmpty: function () {
            return this.buttons.getCurrent() == this.buttons.getNext() && this.buttons.getNext() == this.buttons.getPrevious();
        },

        participate: function (target, eventHandler) {
            console.log("Participate:", target, eventHandler);
            this.buttons.setSelectedButton(this.buttons.getCurrent());
            var self = this;
            $(target).off("keyup");
            $(target).on("keyup", function (ev) {
                if((ignoreKeyboard === undefined) || ignoreKeyboard !== undefined && ignoreKeyboard)
                    return;
                switch (ev.keyCode) {
                    case 40:
                        self.buttons.setSelectedButton(self.buttons.getNext());
                        break;
                    case 38:
                        self.buttons.setSelectedButton(self.buttons.getPrevious());
                        break;
                    case 13:
                        eventHandler.handleEnter();
                        break;
                    case 27:
                        eventHandler.handleEsc();
                        break;
                    default:
                        break;
                }
            });
            var but = $(".button");
            but.hover(function(){
                self.buttons.setSelectedButton($(this));
            });

            but.click(function(){
               eventHandler.handleEnter();
            });

        }
    }
});