/**
 * Created by kuusri on 6/8/2016.
 */

define("util/wiring", ["declare"], function (declare) {
    return declare("util.wiring", [], {
        setCoordinator: function (obj) {
            obj.wiring = [];
            Coordinator = obj;
        },
        broadcast: function (evt, data) {
            Coordinator.wiring.forEach(function (wire) {
                if (wire.eventName == evt) {
                    data.event = evt;
                    wire.target[wire.func].apply(null, [data, wire.target]);
                }
            });
        },
        listen: function (evt, func, target) {
            //console.log("Listen " + evt + " with function " + func + " on ", target);
            var w = Coordinator.wiring;
            var obj = {
                eventName: evt,
                func: func,
                target: target
            }
            var index = w.indexOf(obj);
            if(index < 0)
                w.push(obj);
            else {
                w[index] = obj;
            }
            //console.log("Listening!");
        }

    });
});