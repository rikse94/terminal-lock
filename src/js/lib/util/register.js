/**
 * Created by kuusri on 6/7/2016.
 */
define("util/register", ["declare", "store/Memory"], function(declare, Memory){
    var Register = {}
    Register.elements = {};
    Register.add = function(element){
        var el = $(element)[0];
        if(el.id != ""){
            var obj = this.elements[el.id];
            if(!obj){
                obj = this.elements[el.id] = new Memory();
            }
            el.id += "_" + obj.size();
            obj.put(el);
        }
        return el;
    };
    return declare("util.register", [], Register);
});