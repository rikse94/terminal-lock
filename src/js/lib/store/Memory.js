/**
 * Created by kuusri on 6/7/2016.
 */
define("store/Memory", ["declare"], function (declare) {
    return declare("store.Memory", [], function (args) {
        var Memory = {
            data: [],
            query: function (qry) {
                var matches = [];
                this.data.forEach(function(row){
                    var match = true;
                    for (var k in qry) {
                        if (row[k] !== undefined) {
                            match = match && row[k] == qry[k];
                        }
                    }
                    if(match)
                        matches.push(row);
                });
                return matches;
            },
            setData: function(data){
                if(data){
                    this.data = data;
                }
            },
            put: function(obj){
                if(obj.id === undefined || obj.id == null){
                    obj.id = uuid();
                }
                this.data.push(obj);
            },
            size: function(){
                return this.data.length;
            }
        };
        if(args){
            Memory.data.setData(args.data);
        }
        return Memory;

    });
});