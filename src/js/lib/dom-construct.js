define("dom-construct", [], function(){
   return {
       placeAt: function(source, target){
           $(target).append($(source));
       },
       create: function(tag, args, target){
           var el = document.createElement(tag);
           for(var k in args){
               if(["innerHTML"].includes(k)){
                   el[k] = args[k];
               } else {
                   el.setAttribute(k, args[k]);
               }

               //console.log({el: el});
           }
           this.placeAt(el, target);
           return el;
       }
   }
});