/**
 * Created by kuusri on 6/8/2016.
 */
define("declare", [], function () {

    /*
     a: FQDN for module. E.g. util.register
     b: Object array containing mixins that will extend the result module
     c: target object

     Properties in target object won't be overwritten by mixins.

     example:
     var BaseContext = {a: "A"};
     b = [BaseContext],
     c = {
     constructor: function(){
     console.log("Begin constructor");
     console.log(this.a);
     console.log("End constructor");
     }
     }
     will result as:
     Begin constructor
     A
     End constructor
     */

    return function (a, b, c) {
        b.forEach(function(obj){
            for(var k in obj){
                if(c[k] === undefined){
                    c[k] = obj[k];
                }
            }
        });

        var sa = a.split('.');
        var ref = window;
        for (var i = 0; i < sa.length; i++) {
            if (i != sa.length - 1) {
                if (!ref[sa[i]]) {
                    ref[sa[i]] = {};
                }
                ref = ref[sa[i]];
            } else {
                if(!ref[sa[i]])
                    ref[sa[i]] = c;
            }
        }
        if(c.postCreate){
            c.postCreate();
        }
        return c;
    }
});