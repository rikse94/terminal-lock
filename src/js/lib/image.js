/**
 * @license RequireJS Image Plugin <https://gist.github.com/821476>
 * @author Miller Medeiros
 * @version 0.0.4 (2010/02/14)
 * Released under the MIT License <http://www.opensource.org/licenses/mit-license.php>
 */
(function () {

    var CACHE_BUST_QUERY_PARAM = 'bust',
        CACHE_BUST_FLAG = '!bust';

    function cacheBust(url) {
        url = url.replace(CACHE_BUST_FLAG, '');
        url += (url.indexOf('?') < 0) ? '?' : '&';
        return url + CACHE_BUST_QUERY_PARAM + '=' + (new Date()).getTime();
    }

    function parseName(name) {
        var modName, ext, temp,
            strip = false,
            index = name.lastIndexOf("."),
            isRelative = name.indexOf('./') === 0 ||
                name.indexOf('../') === 0;

        if (index !== -1 && (!isRelative || index > 1)) {
            modName = name.substring(0, index);
            ext = name.substring(index + 1);
        } else {
            modName = name;
        }

        temp = ext || modName;
        index = temp.indexOf("!");
        if (index !== -1) {
            //Pull off the strip arg.
            strip = temp.substring(index + 1) === "strip";
            temp = temp.substring(0, index);
            if (ext) {
                ext = temp;
            } else {
                modName = temp;
            }
        }

        return {
            moduleName: modName,
            ext: ext,
            strip: strip
        };
    }

    //as of RequireJS 0.22 - define method for plugins needs to be a literal object
    //to be able to work together with the optimizer (see: https://github.com/jrburke/requirejs/issues#issue/70)
    define({
        load: function (name, req, onLoad, config) {
            var img;
            if (config.isBuild) {
                onLoad(null); //avoid errors on the optimizer since it can't inline image files.
            } else {
                img = new Image();
                img.onload = function (evt) {
                    onLoad(img);
                    delete img.onload; //release memory - suggested by John Hann
                };

                var parsed = parseName(name),
                    nonStripName = parsed.moduleName +
                        (parsed.ext ? '.' + parsed.ext : ''),
                    url = req.toUrl(nonStripName);
                img.src = url;
            }
        },
        normalize: function (name, normalize) {
            //used normalize to avoid caching references to a "cache busted" request.
            return (name.indexOf(CACHE_BUST_FLAG) < 0) ? name : cacheBust(name);
        }
    });

}());