define("parser", [], function(){

    return function(template, strings){
        console.log("Strings: ", strings);
        var matches = template.match(/\$\{.*?\}/g);
        if(matches != null){
            matches.forEach(function(m){
                var key = m.replace(/[\{\}\$]/g, "");
                var span = "<span class='write' id='" + key + "'>" + strings[key] + "</span>";
                span = util.register.add(span);
                template = template.replace(m, span.outerHTML);
            });
        }
        return template;
    }
});