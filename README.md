#Terminal Lock
A replica of [terminal lock][1] seen in Fallout branch games.

##Description
Terminal lock is Fallout style terminal lock which will operate in co-operation with external controller.

##Purpose
Terminal lock project is started initially as part of larger Room Escape SAMK project.
Terminal lock is one of mini-puzzles for unlocking a door.


[1]: http://vignette2.wikia.nocookie.net/fallout/images/7/74/Terminal.jpg/revision/latest?cb=20090103013618